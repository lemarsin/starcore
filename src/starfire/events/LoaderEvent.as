//------------------------------------------------------------------------------
//
// interface: IModel
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-21
// copyright (c) 2013 小兵( aosnow@yeah.net )
// 加载进度事件
//
//------------------------------------------------------------------------------

package starfire.events
{

	import starfire.rpc.loader.LoadMessage;
	import starfire.rpc.loader.elements.*;

	import flash.events.Event;

	/**
	 * 资源加载状态事件
	 * 
	 * <p>
	 * 加载状态，定义在 <code>LoaderState</code> 中，包含：
	 * <ul>
	 * <li><code>UNINITIALIZED</code> - 新创建的加载信息（LoadMessage）的初始状态</li>
	 * <li><code>LOADING</code> - 开始并正处在加载过程</li>
	 * <li><code>UNLOADING</code> - 正处在卸载过程中</li>
	 * <li><code>READY</code> - 已经加载完成，准备好被使用</li>
	 * <li><code>LOAD_ERROR</code> - 加载错误（如加载地址错误、网络原因等）</li>
	 * </ul>
	 * </p>
	 * 
	 * <p><b>注意</b>：<code>LoaderEvent</code> 继承于 <code>flash.events.Event</code>，属于Flash平台的事件。</p>
	 * 
	 * @see starfire.rpc.loader.LoadState
	 * 
	 * @author AoSnow
	 */	
	public class LoaderEvent extends Event
	{

		//--------------------------------------------------------------------------
		//
		//   Class constants
		//
		//--------------------------------------------------------------------------

		public static const LOAD_STATE_CHANGE:String = "loadStateChange";

		//--------------------------------------------------------------------------
		//
		//   Class properties
		//
		//--------------------------------------------------------------------------

		private var _loader:LoaderBase;
		private var _loadMessage:LoadMessage;
		private var _oldState:String;
		private var _newState:String;

		/**
		 * 加载任务所使用的加载器
		 */
		public function get loader():LoaderBase
		{
			return _loader;
		}

		/**
		 * 加载任务的相关提交数据
		 */
		public function get loadMessage():LoadMessage
		{
			return _loadMessage;
		}

		/**
		 * 加载数据的前一个状态
		 */
		public function get oldState():String
		{
			return _oldState;
		}

		/**
		 * 加载数据的最新状态
		 */
		public function get newState():String
		{
			return _newState;
		}

		//--------------------------------------------------------------------------
		//
		//   Class constructor
		//
		//--------------------------------------------------------------------------

		public function LoaderEvent( type:String, //
									 loader:LoaderBase = null, //
									 loadMessage:LoadMessage = null, //
									 oldState:String = null, //
									 newState:String = null )
		{
			super( type, false, false );

			_loader = loader;
			_loadMessage = loadMessage;
			_oldState = oldState;
			_newState = newState;
		}

		override public function clone():Event
		{
			return new LoaderEvent( type, loader, loadMessage, oldState, newState );
		}

	}
}
