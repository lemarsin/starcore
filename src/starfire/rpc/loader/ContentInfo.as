//------------------------------------------------------------------------------
//
// ...
// classname: ContentInfo
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-3-30
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.rpc.loader
{
	
	/**
	 * 加载对象
	 * @author AoSnow
	 * 
	 */	
	public class ContentInfo
	{
		/** 资源类型 **/		
		public var type:String;
		
		/** 唯一标识名（使用此值进行检索）**/
		public var name:String;
		
		/** 远程加载服务地址  **/
		public var url:String;
		
		/** 资源内容 **/		
		public var content:*;
		
		/**
		 * 资源加载后所存放的组
		 * <p>资源加载的自定义组名，可以统一的批量释放某一个组的所有类型资源。</p>
		 */
		public var group:String;
	}
}