//------------------------------------------------------------------------------
//
//   author: 小兵（aosnow@yeah.net）
//   create: 2012-5-29 下午22:36:19
//    class: VideoLoader - 视频数据加载类
//
//------------------------------------------------------------------------------

package starfire.rpc.loader.elements
{
	import flash.events.Event;
	
	import starfire.rpc.URL;
	import starfire.rpc.loader.LoadMessage;

	[ExcludeClass]
	public class VideoLoader extends LoaderBase
	{
		public function VideoLoader( message:LoadMessage )
		{
			super( message );
		}

		override public function canHandleResource():Boolean
		{
			if( super.message )
			{
				var url:URL = new URL( super.message.url );
				return ( url.path.search( /\.flv$|\.wmv$|\.asf$|\.mpg$|\.mpeg$/i ) != -1 );
			}

			return false;
		}

		override protected function executeLoad( message:LoadMessage ):void
		{
		}

		override protected function executeUnload( message:LoadMessage ):void
		{
		}

		override protected function loadCompleteHandler( event:Event ):void
		{
		}
	}
}
