//------------------------------------------------------------------------------
//
//   author: 小兵（aosnow@yeah.net）
//   create: 2012-5-29 下午4:14:32
//    class: LoadMessage - 加载任务信息描述，用于启动一条加载命令
//
//------------------------------------------------------------------------------

package starfire.rpc.loader
{
	import flash.system.ApplicationDomain;

	public class LoadMessage
	{

		//--------------------------------------------------------------------------
		//
		//   属性列表
		//
		//--------------------------------------------------------------------------

		/** 加载对象类型  **/
		public var mediaType:String;

		/** 唯一标识名（使用此值进行检索）**/
		public var name:String;

		/** 远程加载服务地址  **/
		public var url:String;

		/** 加载的模块存放的空间，NULL 表示将加载的内容合并到当前应用程序域中（此属性只对加载SWF内容时生效）  **/
		public var domain:ApplicationDomain;

		/** 加载过程中需要显示的提示信息：如“角色资源、角色头像”等描述信息  **/
		public var summary:String;

		/**
		 * 资源加载后所存放的组
		 * <p>资源加载的自定义组名，可以统一的批量释放某一个组的所有类型资源。</p>
		 */
		public var group:String;

		/**
		 * 用于将加载SWF后的字节数据进行解码，若SWF源是未经加密的，
		 * 可省略 decryption 解密函数的设置，函数执行时将传入一个字节数组参数，如：
		 * <pre>
		 * function decryption(bytes:ByteArray):ByteArray
		 * {
		 * 	// ... 此处对 bytes 进行解密后并返回
		 * 	return newBytes;
		 * }
		 * </pre>
		 */
		public var decryption:Function;
		
		/**
		 * 加载完成后的返回数据，这根据加载类型来决定
		 */
		public var result:*;

		/**
		 * 加载资源的特征描述
		 * @param name					- 资源标识，必须保证在同类型资源中的唯一性，才能正确索引取得加载结果
		 * @param url					- 资源地址（必须保证正确的文件后缀结尾，否则将引发非法操作错误）
		 * @param mediaType				- 资源类型
		 * @param group					- 资源加载的自定义组名，可以统一的批量释放某一个组的所有类型资源
		 * @param domain				- 加载后存放的域（只适用于SWF媒体加载，其它资源类型可不设置）
		 * @param summary				- 加载过程中显示的提示信息（可设置为资源名称，如：“角色资源”，将显示类似这样的结果
		 * “正在加载  角色资源... 90%”）
		 */
		public function LoadMessage( name:String = null, // 
									 url:String = null, // 
									 mediaType:String = "image", // 
									 group:String = "temp", //
									 domain:ApplicationDomain = null, // 
									 summary:String = "", //
									 decryption:Function = null )
		{
			this.name = name;
			this.url = url;
			this.mediaType = mediaType;
			this.group = group;
			this.domain = domain;
			this.summary = summary;
			this.decryption = decryption;
		}

		//--------------------------------------------------------------------------
		//
		//   加载状态的改变
		//
		//--------------------------------------------------------------------------

		private var _loadState:String;

		/** 加载过程中的状态  **/
		public function get loadState():String
		{
			return _loadState;
		}

		public function set loadState( value:String ):void
		{
			_loadState = value;
		}

	}
}
