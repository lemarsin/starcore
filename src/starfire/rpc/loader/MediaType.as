//------------------------------------------------------------------------------
//
//   author: 小兵（aosnow@yeah.net）
//   create: 2012-5-29 下午5:11:19
//    class: MediaType - 加载资源的类型常量
//
//------------------------------------------------------------------------------

package starfire.rpc.loader
{

	/**
	 * 枚举媒体类型的常量
	 */
	public final class MediaType
	{
		/**
		 * 视频类型（暂不支持加载）
		 */
		public static const VIDEO:String = "video";

		/**
		 * 音频类型
		 */
		public static const AUDIO:String = "audio";

		/**
		 * 图像类型
		 */
		public static const IMAGE:String = "image";

		/**
		 * SWF媒体类型
		 */
		public static const SWF:String = "swf";

		/**
		 * 字节数组类型（以二进制下载的字节数据）
		 */
		public static const BINARY:String = "binary";
		
		/**
		 * XML类型（若不是规范的XML类型，则会抛出错误）
		 */
		public static const XML:String = "xml";
		
		/**
		 * Text纯字符串文本类型
		 */
		public static const TEXT:String = "text";
	}
}
