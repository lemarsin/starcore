//------------------------------------------------------------------------------
//
// ...
// classname: Core
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-3-31
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.cores
{
	import starfire.handlers.Handler;
	import starfire.rpc.loader.LoadAgent;
	import starfire.rpc.loader.LoadMessage;

	/**
	 * 框架核心方法类
	 * @author AoSnow
	 */
	public class Core
	{
		//--------------------------------------------------------------------------
		//
		//   Class variable
		//
		//--------------------------------------------------------------------------

		/** 加载任务代理  **/
		protected var loadAgent:LoadAgent;

		//--------------------------------------------------------------------------
		//
		//   Class constructor
		//
		//--------------------------------------------------------------------------

		private static var _instance:Core;

		public function Core()
		{
			if( _instance != null )
				throw new Error( "请通过 getInstance() 方法调用唯一实例！" );

			loadAgent = new LoadAgent();
		}

		public static function getInstance():Core
		{
			if( _instance == null )
				_instance = new Core;

			return _instance;
		}

		//--------------------------------------------------------------------------
		//
		//   加载单个资源，即时开始加载
		//
		//--------------------------------------------------------------------------

		/**
		 * 根据 message 的描述信息加载指定的资源（单个加载，即时启动加载，要进行队列加载请使用 loadList）
		 * @param message				- 描述信息
		 * @param complete				- 完成时的回调函数，参数为 取得的相应数据
		 * @param process				- 加载进度时的回调函数，参数为 flash.events.ProgressEvent
		 *
		 * <p>
		 * 成功返回结果时，参数类型如下：
		 * <ul>
		 * <li>MediaType.VIDEO	- 暂不支持视频内容加载</li>
		 * <li>MediaType.AUDIO	- 返回结果为 Sound，回调方法参数示例：<br/><code>function(sound:Sound)</code></li>
		 * <li>MediaType.IMAGE	- 返回结果为原始 LoadMessage，其 result 将存储所加载的 BitmapData 返回，
		 * 回调方法参数示例：<br/><code>function(message:LoadMessage)</code></li>
		 * <li>MediaType.SWF	- 返回结果为包含 SWF 内容的LoaderInfo，回调方法参数示例：<br/><code>function(content:LoaderInfo)</code></li>
		 * <li>MediaType.BINARY	- 返回结果为 ByteArray，回调方法参数示例：<br/><code>function(byteArray:ByteArray)</code></li>
		 * <li>MediaType.TEXT	- 返回结果为 String，回调方法参数示例：<br/><code>function(text:String)</code></li>
		 * <li>MediaType.XML	- 返回结果为 XML，回调方法参数示例：<br/><code>function(xml:XML)</code></li>
		 * </ul>
		 * </p>
		 *
		 * @see starfire.rpc.loader.LoadMessage
		 * @see flash.events.ProgressEvent
		 */
		public function load( message:LoadMessage, complete:Function = null, process:Function = null ):void
		{
			loadAgent.load( message, complete, process );
		}

		//--------------------------------------------------------------------------
		//
		//   以队列的形式进行资源加载
		//
		//--------------------------------------------------------------------------

		/**
		 * 根据 messages 队列的描述信息加载指定的资源（队列加载，初始完队列设置后自动开始启动加载）
		 * <p>若以下 Handler 类型的回调方法增加了自定义参数，将合并到以下所描述的标准参数之前，同时返回</p>
		 * @param messages				- 描述信息队列
		 * @param complete				- 完成时的回调函数，参数为 [ 实际加载的数量, 完成数量, 总数量 ]（若任务中包含已经加载过的任务，则会被忽略，这时实际加载数量就会减 1）
		 * @param process				- 总体加载进度时的回调函数，参数为 [ 完成数量, 总数量 ]
		 * @param singleProcess			- 单个加载任务加载进度时的回调函数，参数为正在处理的任务的 ProgressEvent
		 *
		 * <p>若要取得加载数据，请从 MediaCache 类的缓存中获取，这需要用到加载描述信息中所设置的名称标识。</p>
		 */
		public function loadList( messages:Vector.<LoadMessage>, complete:Handler = null, process:Handler = null, singleProcess:Function = null ):void
		{
			loadAgent.loadList( messages, complete, process, singleProcess );
		}
	}
}
