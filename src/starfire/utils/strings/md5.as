//------------------------------------------------------------------------------
//
// classname: md5
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{
	import starfire.utils.MD5;

	/**
	 * 计算给定数据的MD5值
	 * @param value		- 给定数据，任意数据（非字符串类型将被强制转换成字符串类型）
	 * @return 返回 MD5 值
	 */
	public function md5( value:Object ):String
	{
		return MD5.CapitalHash( value.toString());
	}
}
