//------------------------------------------------------------------------------
//
// classname: hash
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{
	/**
	 * 创建一个指定长度由随机字符组成的哈希值字符串（随机字符串，用以替代MD5实现更高效的随机码）
	 * 原则上不会出现连续相同字符
	 * @param length				- 长度（3~128）
	 * @param type					- 结果类型: 0 - 纯数字字符串;1-数字和字母混杂字符串（默认：1）
	 * @param lowerCase				- 是否返回全小写字母字符串（默认：false，返回全大写字母结果）
	 */
	public function hash( length:uint = 8, type:uint = 1, lowerCase:Boolean = false ):String
	{
		var max:uint = HASHDIC.length; // 字符库元素个数
		var tmp:String = ""; // 缓存结果

		if( type == 0 )
			max = 10;

		if( length < 3 || length > 128 )
			length = 8;

		for( var i:int; i < length; i++ )
		{
			tmp += HASHDIC.charAt( Math.random() * max >> 0 );
		}

		if( !lowerCase )
			tmp = tmp.toUpperCase();

		return tmp;
	}
}

const HASHDIC:String = "0123456789abcdefghijklmnopqrstuvwxyz_";
