//------------------------------------------------------------------------------
//
// 重复字符叠加方法
// filename: repeat.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-4-21
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{

	/**
	 *  将指定的字符重复指定次数叠加在一起创建新的字符串
	 *  @param str					- 需要进行重复的字符串单位
	 *  @return						- 重复叠加后的新字符串
	 */
	public function repeat( str:String, times:int = 3 ):String
	{
		var r:String = "";

		for( var i:int = 0; i < times; i++ )
		{
			r += str;
		}

		return r;
	}
}
