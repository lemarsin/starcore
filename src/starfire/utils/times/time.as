//------------------------------------------------------------------------------
//
// ...
// filename: time.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-11-4
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.times
{
	/**
	 * 返回自从 Unix 纪元（格林威治时间 1970 年 1 月 1 日 00:00:00）到当前时间的毫秒数
	 * @return 秒数
	 */
	public function time():Number
	{
		var date:Date = new Date();
		var millisecond:Number = date.time;// 毫秒数
		date = null;
		
		return millisecond;
	}
}
