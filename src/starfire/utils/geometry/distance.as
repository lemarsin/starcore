//------------------------------------------------------------------------------
//
// 求两个点之间的平面距离
// filename: distance.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-11-10
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.geometry
{

	/**
	 * 求两个点之间的平面距离
	 * <p>这是一个简便方法，效率要比 Point.distance 方法提高将近一倍。而且调用时的代码更简洁。</p>
	 * @param value 需要计算绝对值源数据
	 */
	public function distance( startX:Number = 0, startY:Number = 0, endX:Number = 0, endY:Number = 0 ):Number
	{
		return Math.sqrt( Math.pow( endX - startX, 2 ) + Math.pow( endY - startY, 2 ));
	}
}
