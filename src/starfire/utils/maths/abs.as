//------------------------------------------------------------------------------
//
// 求一个数值的绝对值
// filename: abs.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-1-3
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.maths
{

	/**
	 * 求一个数值的绝对值
	 * <p>这是一个简便方法，效率要比 Math.abs 快很多。而且调用时的代码更简洁。</p>
	 * @param value 需要计算绝对值源数据
	 */
	public function abs( value:Number ):Number
	{
		return value > 0 ? value : value * -1;
	}
}
