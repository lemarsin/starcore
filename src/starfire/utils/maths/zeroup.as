//------------------------------------------------------------------------------
//
// ...
// filename: zeroup.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-11-4
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.maths
{
	/**
	 * 将指定数值补足指定个数的前置0
	 * @param value 需要补0的数值
	 * @param length 返回字符串的长度，决定补0的个数
	 * @return 数值字符串，第一位可能是0
	 */
	public function zeroup( value:Number, length:int = 2 ):String
	{
		var r:String = String( value );

		if( r.length == 2 )
			return r;

		while( r.length < length )
		{
			r = "0" + r;
		}

		return r;
	}
}
