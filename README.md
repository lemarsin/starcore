starcore v1.0
=====
-----

` 喵大斯核心库（含http、loader应用，及json、md5、文本模板、日期等大量工具方法）`
` 喵大斯诸多开源作品，都会基于此基础库进行开发，喜欢我作品的朋友请务必下载使用本基础库。`

##这是一个作为底层通信、常用工具方法的经典库：
  * 1、web service 通信封装
  * 2、远程加载库支持 ImageLoader、SWFLoader、XMLLoader、TextLoader、AudioLoader、BinaryLoader
  * 3、远程加载缓存管理 MediaCache，可以方便的获取和删除缓存资源，并且可对混合类型的资源进行分批管理（页游开发中常见的需要按场景对资源进行加载和清除）
  * 4、字符串模板 printf()，支持顺序替换和全局替换
  * 5、字符串处理 md5、hash、json/jsond、trim、repeat 等
  * 6、数学计算 abs、max、min、round/roundx 等高效方法替代 Math 包方法
  * 7、滤镜工具 生成和调整颜色、变换滤镜 ColorFilter
  * 8、日期时间处理 datediff、strdate、time、timens
  * 9、位图切割工具 BitmapUtil
  * 10、阻止WEB加速器的计时器 SuperTimer
  * 11、调试输出工具 Debug，单独运行无依赖

##开发计划：
  * 1、增加 Socket 通信及通信协议支持